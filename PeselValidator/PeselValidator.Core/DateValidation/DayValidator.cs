using System.Collections.Generic;

namespace PeselValidator.Core.DateValidation
{
    public class DayValidator
    {
        private const int February = 2;
        private const int MinDayLimit = 1;
        private static int _month;
        private static int _year;

        private static Dictionary<int, int> _monthDay = new Dictionary<int, int>()
        {
            [1]= 31,
            [3]= 31,
            [4]= 30,
            [5]= 31,
            [6]= 30,
            [7]= 31,
            [8]= 31,
            [9]= 30,
            [10]= 31,
            [11]= 30,
            [12]= 31,
        };

        private static int MaxDayLimit()
        {
            if (_month == February)
                return (YearValidator.IsLeapYear(_year) ? 29 : 28);

            if (_monthDay.ContainsKey(_month))
                return _monthDay[_month];

            return 0;
        }


        public static bool ValidateDay(int year, int month, int day)
        {
            _month = month;
            _year = year;

            return (day >= MinDayLimit && day <= MaxDayLimit());
        } 

    }
}